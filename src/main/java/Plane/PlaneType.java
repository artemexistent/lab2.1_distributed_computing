package Plane;

public enum PlaneType {
  support,
  escort,
  fighter,
  interceptor,
  reconnaissance
}
