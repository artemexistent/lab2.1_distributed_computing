import Parser.XMLValidator;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class XMLValidatorTest {

    @Test
    public void validXMLvalidation(){
        assertTrue(XMLValidator.validateXML("src/test/resources/example.xml",
                "src/main/resources/Plane.xsd"));
    }

    @Test
    public void invalidXMLvalidation(){
        assertFalse(XMLValidator.validateXML("src/test/resources/invalid.xml",
                "src/main/resources/Plane.xsd"));
    }
}
